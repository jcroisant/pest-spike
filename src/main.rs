#[macro_use]
extern crate pest;
#[macro_use]
extern crate pest_derive;


#[derive(Parser)]
#[grammar = "grammar.pest"]
pub struct ExprParser;


#[cfg(debug_assertions)]
const _GRAMMAR: &str = include_str!("grammar.pest");


fn main() {}


#[cfg(test)]
mod tests {
    use super::*;

    macro_rules! parse_datum {
        ($input:expr, $tokens:tt) => {
            parses_to! {
                parser: ExprParser,
                input: $input,
                rule: Rule::datum,
                tokens: $tokens
            }
        }
    }

    #[test]
    fn btrue() {
        parse_datum!{"#t",    [btrue(0, 2)]};
        parse_datum!{"#T",    [btrue(0, 2)]};
        parse_datum!{"#true", [btrue(0, 5)]};
        parse_datum!{"#TRUE", [btrue(0, 5)]};
        parse_datum!{"#tRuE", [btrue(0, 5)]};
    }

    #[test]
    fn bfalse() {
        parse_datum!{"#f",     [bfalse(0, 2)]};
        parse_datum!{"#F",     [bfalse(0, 2)]};
        parse_datum!{"#false", [bfalse(0, 6)]};
        parse_datum!{"#FALSE", [bfalse(0, 6)]};
        parse_datum!{"#fAlSe", [bfalse(0, 6)]};
    }

    #[test]
    fn bv_empty() {
        parse_datum!{"#u8()", [bytevec(0,5)]};
    }

    #[test]
    fn bv_simple() {
        parse_datum!{"#u8(0)",   [bytevec(0,6, [int(4,5, [int10(4,5)])])]};
        parse_datum!{"#u8( 0 )", [bytevec(0,8, [int(5,6, [int10(5,6)])])]};
        parse_datum!{"#u8(255)", [bytevec(0,8, [int(4,7, [int10(4,7)])])]};
        parse_datum!{
            "#u8(0 64 128 255)",
            [bytevec(0,17, [int(4,5,   [int10(4,5)]),
                            int(6,8,   [int10(6,8)]),
                            int(9,12,  [int10(9,12)]),
                            int(13,16, [int10(13,16)])])]
        };
    }

    #[test]
    fn bv_radix() {
        parse_datum!{"#u8(#b0 #b11111111)",
                     [bytevec(0,19, [int(4,7,  [int2(6,7)]),
                                     int(8,18, [int2(10,18)])])]};
        parse_datum!{"#u8(#o0 #o377)",
                     [bytevec(0,14, [int(4,7,  [int8(6,7)]),
                                     int(8,13, [int8(10,13)])])]};
        parse_datum!{"#u8(#d0 #d255)",
                     [bytevec(0,14, [int(4,7,  [int10(6,7)]),
                                     int(8,13, [int10(10,13)])])]};
        parse_datum!{"#u8(#x0 #xff)",
                     [bytevec(0,13, [int(4,7,  [int16(6,7)]),
                                     int(8,12, [int16(10,12)])])]};
        parse_datum!{"#u8(#b10 #o10 #d10 #x10)",
                     [bytevec(0,24, [int(4,8,   [int2(6,8)]),
                                     int(9,13,  [int8(11,13)]),
                                     int(14,18, [int10(16,18)]),
                                     int(19,23, [int16(21,23)])])]};
    }

    #[test]
    fn bv_ratio() {
        parse_datum!{"#u8(0/1 255/1)",
                     [bytevec(0,14, [ratio(4,7,  [int10(4,5),  int10(6,7)]),
                                     ratio(8,13, [int10(8,11), int10(12,13)])])]};
        parse_datum!{"#u8(-255/-1)",
                     [bytevec(0,12, [ratio(4,11, [int10(4,8),  int10(9,11)])])]};
        parse_datum!{"#u8(0/1000 255000/1000)",
                     [bytevec(0,23, [ratio(4,10,  [int10(4,5),   int10(6,10)]),
                                     ratio(11,22, [int10(11,17), int10(18,22)])])]};

        parse_datum!{"#u8(#b101/11)",
                     [bytevec(0,13, [ratio(4,12, [int2(6,9),  int2(10,12)])])]};
        parse_datum!{"#u8(#o377/-7)",
                     [bytevec(0,13, [ratio(4,12, [int8(6,9),  int8(10,12)])])]};
        parse_datum!{"#u8(#x-ff/af)",
                     [bytevec(0,13, [ratio(4,12, [int16(6,9), int16(10,12)])])]};
    }

    #[test]
    fn bv_out_of_range() {
        parse_datum!{"#u8(-1)",  [bytevec(0,7, [int(4,6, [int10(4,6)])])]};
        parse_datum!{"#u8(256)", [bytevec(0,8, [int(4,7, [int10(4,7)])])]};

        parse_datum!{"#u8(#b-1)", [bytevec(0,9, [int(4,8, [int2(6,8)])])]};
        parse_datum!{"#u8(#b100000000)",
                     [bytevec(0,16, [int(4,15, [int2(6,15)])])]};

        parse_datum!{"#u8(#o-1)",  [bytevec(0,9,  [int(4,8, [int8(6,8)])])]};
        parse_datum!{"#u8(#o400)", [bytevec(0,10, [int(4,9, [int8(6,9)])])]};

        parse_datum!{"#u8(#d-1)",  [bytevec(0,9,  [int(4,8, [int10(6,8)])])]};
        parse_datum!{"#u8(#d256)", [bytevec(0,10, [int(4,9, [int10(6,9)])])]};

        parse_datum!{"#u8(#x-1)",  [bytevec(0,9,  [int(4,8, [int16(6,8)])])]};
        parse_datum!{"#u8(#x100)", [bytevec(0,10, [int(4,9, [int16(6,9)])])]};

        parse_datum!{"#u8(-10/10)",
                     [bytevec(0,11, [ratio(4,10, [int10(4,7), int10(8,10)])])]};
        parse_datum!{"#u8(512/2)",
                     [bytevec(0,10, [ratio(4,9, [int10(4,7), int10(8,9)])])]};
    }

    #[test]
    fn bv_bad_contents() {
        parse_datum!{"#u8(0.0)",   [bytevec(0,8,  [float(4,7, [coeff(4,7)])])]};
        parse_datum!{"#u8(255.0)", [bytevec(0,10, [float(4,9, [coeff(4,9)])])]};
        parse_datum!{"#u8(25e1)",
                     [bytevec(0,9,  [float(4,8, [coeff(4,6), expon(7,8)])])]};

        parse_datum!{"#u8(1/10)",
                     [bytevec(0,9, [ratio(4,8, [int10(4,5), int10(6,8)])])]};
        parse_datum!{"#u8(33/2)",
                     [bytevec(0,9, [ratio(4,8, [int10(4,6), int10(7,8)])])]};

        parse_datum!{"#u8(a)",    [bytevec(0,6, [ident(4,5)])]};
        parse_datum!{"#u8(\"\")", [bytevec(0,7, [string(4,6)])]};
        parse_datum!{"#u8(())",   [bytevec(0,7, [list(4,6)])]};

        parse_datum!{"#u8(#u8())", [bytevec(0,10, [bytevec(4,9)])]};
    }


    #[test]
    fn char_alphanumeric() {
        parse_datum!{r"#\a", [character(0,3, [ch_any(2,3)])]};
        parse_datum!{r"#\A", [character(0,3, [ch_any(2,3)])]};
        parse_datum!{r"#\x", [character(0,3, [ch_any(2,3)])]};
        parse_datum!{r"#\X", [character(0,3, [ch_any(2,3)])]};
        parse_datum!{r"#\z", [character(0,3, [ch_any(2,3)])]};
        parse_datum!{r"#\Z", [character(0,3, [ch_any(2,3)])]};
        parse_datum!{r"#\0", [character(0,3, [ch_any(2,3)])]};
        parse_datum!{r"#\1", [character(0,3, [ch_any(2,3)])]};
        parse_datum!{r"#\9", [character(0,3, [ch_any(2,3)])]};
    }

    #[test]
    fn char_symbol() {
        parse_datum!{r"#\ ", [character(0,3, [ch_any(2,3)])]};
        parse_datum!{r"#\\", [character(0,3, [ch_any(2,3)])]};
        parse_datum!{r"#\(", [character(0,3, [ch_any(2,3)])]};
        parse_datum!{r"#\)", [character(0,3, [ch_any(2,3)])]};
        parse_datum!{r"#\+", [character(0,3, [ch_any(2,3)])]};
        parse_datum!{r"#\.", [character(0,3, [ch_any(2,3)])]};
        parse_datum!{r"#\,", [character(0,3, [ch_any(2,3)])]};
        parse_datum!{r"#\;", [character(0,3, [ch_any(2,3)])]};
        parse_datum!{r"#\#", [character(0,3, [ch_any(2,3)])]};
        parse_datum!{r"#\|", [character(0,3, [ch_any(2,3)])]};

        parse_datum!{"#\\\t", [character(0,3, [ch_any(2,3)])]};
        parse_datum!{"#\\\n", [character(0,3, [ch_any(2,3)])]};
        parse_datum!{"#\\\"", [character(0,3, [ch_any(2,3)])]};
    }

    #[test]
    fn char_unicode() {
        parse_datum!{r"#\λ", [character(0,4, [ch_any(2,4)])]};
        parse_datum!{r"#\🐱", [character(0,6, [ch_any(2,6)])]};
        parse_datum!{r"#\語", [character(0,5, [ch_any(2,5)])]};
    }

    #[test]
    fn char_hex() {
        parse_datum!{r"#\x9",     [character(0,4, [ch_hex(3,4)])]};
        parse_datum!{r"#\x1f431", [character(0,8, [ch_hex(3,8)])]};

        parse_datum!{r"(#\x #\x(#\X))",
                     [list(0,14, [
                         character(1,4, [ch_any(3,4)]),
                         character(5,8, [ch_any(7,8)]),
                         list(8,13, [character(9,12, [ch_any(11,12)])])
                     ])]};
    }

    #[test]
    fn char_named() {
        parse_datum!{r"#\alarm",     [character(0,7,  [ch_name(2,7)])]};
        parse_datum!{r"#\backspace", [character(0,11, [ch_name(2,11)])]};
        parse_datum!{r"#\delete",    [character(0,8,  [ch_name(2,8)])]};
        parse_datum!{r"#\escape",    [character(0,8,  [ch_name(2,8)])]};
        parse_datum!{r"#\newline",   [character(0,9,  [ch_name(2,9)])]};
        parse_datum!{r"#\null",      [character(0,6,  [ch_name(2,6)])]};
        parse_datum!{r"#\return",    [character(0,8,  [ch_name(2,8)])]};
        parse_datum!{r"#\space",     [character(0,7,  [ch_name(2,7)])]};
        parse_datum!{r"#\tab",       [character(0,5,  [ch_name(2,5)])]};
    }

    #[test]
    fn integerish() {
        parse_datum!{"12+34i", [complex(0,6, [creal(0,2, [coeff(0,2)]),
                                              cimag(2,5, [coeff(2,5)])])]};
        parse_datum!{"12+34I", [complex(0,6, [creal(0,2, [coeff(0,2)]),
                                              cimag(2,5, [coeff(2,5)])])]};
        parse_datum!{"12-34i", [complex(0,6, [creal(0,2, [coeff(0,2)]),
                                              cimag(2,5, [coeff(2,5)])])]};
        parse_datum!{"12-34I", [complex(0,6, [creal(0,2, [coeff(0,2)]),
                                              cimag(2,5, [coeff(2,5)])])]};
    }


    #[test]
    fn complex_floatish() {
        parse_datum!{"1.2+3.4i", [complex(0,8, [creal(0,3, [coeff(0,3)]),
                                                cimag(3,7, [coeff(3,7)])])]};
        parse_datum!{"1.2+3.4I", [complex(0,8, [creal(0,3, [coeff(0,3)]),
                                                cimag(3,7, [coeff(3,7)])])]};
        parse_datum!{"1.2-3.4i", [complex(0,8, [creal(0,3, [coeff(0,3)]),
                                                cimag(3,7, [coeff(3,7)])])]};
        parse_datum!{"1.2-3.4I", [complex(0,8, [creal(0,3, [coeff(0,3)]),
                                                cimag(3,7, [coeff(3,7)])])]};
    }

    #[test]
    fn complex_exponent() {
        parse_datum!{"1.2e-1+3.4e3i",
                     [complex(0,13, [creal(0,6,  [coeff(0,3),  expon(4,6)]),
                                     cimag(6,12, [coeff(6,10), expon(11,12)])])]};
        parse_datum!{"1.2e-1-3.4e3i",
                     [complex(0,13, [creal(0,6,  [coeff(0,3),  expon(4,6)]),
                                     cimag(6,12, [coeff(6,10), expon(11,12)])])]};
        parse_datum!{"1.2E-1+3.4E3i",
                     [complex(0,13, [creal(0,6,  [coeff(0,3),  expon(4,6)]),
                                     cimag(6,12, [coeff(6,10), expon(11,12)])])]};
        parse_datum!{"1.2e-1+3.4e3I",
                     [complex(0,13, [creal(0,6,  [coeff(0,3),  expon(4,6)]),
                                     cimag(6,12, [coeff(6,10), expon(11,12)])])]};

        parse_datum!{"1.2e-1+34i",
                     [complex(0,10, [creal(0,6, [coeff(0,3), expon(4,6)]),
                                     cimag(6,9, [coeff(6,9)])])]};
        parse_datum!{"12+3.4e3i",
                     [complex(0,9, [creal(0,2, [coeff(0,2)]),
                                    cimag(2,8, [coeff(2,6), expon(7,8)])])]};
    }

    #[test]
    fn complex_radix() {
        parse_datum!{"#d12+34i", [complex(0,8, [creal(2,4, [coeff(2,4)]),
                                                cimag(4,7, [coeff(4,7)])])]};
        parse_datum!{"#D12-34i", [complex(0,8, [creal(2,4, [coeff(2,4)]),
                                                cimag(4,7, [coeff(4,7)])])]};
        parse_datum!{"#d1.2+3.4i", [complex(0,10, [creal(2,5, [coeff(2,5)]),
                                                   cimag(5,9, [coeff(5,9)])])]};
        parse_datum!{"#D1.2-3.4i", [complex(0,10, [creal(2,5, [coeff(2,5)]),
                                                   cimag(5,9, [coeff(5,9)])])]};
        parse_datum!{"#d1.2e-1+3.4e3i",
                     [complex(0,15, [creal(2,8,  [coeff(2,5),  expon(6,8)]),
                                     cimag(8,14, [coeff(8,12), expon(13,14)])])]};
        parse_datum!{"#D1.2E-1+3.4E3I",
                     [complex(0,15, [creal(2,8,  [coeff(2,5),  expon(6,8)]),
                                     cimag(8,14, [coeff(8,12), expon(13,14)])])]};
    }

    #[test]
    fn complex_infinity() {
        parse_datum!{"1.2-inf.0i",
                     [complex(0,10, [creal(0,3, [coeff(0,3)]),
                                     cimag(3,9, [inf(3,9)])])]};
        parse_datum!{"+inf.0-1.2i",
                     [complex(0,11, [creal(0,6, [inf(0,6)]),
                                     cimag(6,10, [coeff(6,10)])])]};
        parse_datum!{"+inf.0-inf.0i",
                     [complex(0, 13, [creal(0,6,  [inf(0,6)]),
                                      cimag(6,12, [inf(6,12)])])]};
        parse_datum!{"-inf.0+inf.0i",
                     [complex(0, 13, [creal(0,6,  [inf(0,6)]),
                                      cimag(6,12, [inf(6,12)])])]};
        parse_datum!{"+INF.0-INF.0I",
                     [complex(0, 13, [creal(0,6,  [inf(0,6)]),
                                      cimag(6,12, [inf(6,12)])])]};
        parse_datum!{"-INF.0+INF.0I",
                     [complex(0, 13, [creal(0,6,  [inf(0,6)]),
                                      cimag(6,12, [inf(6,12)])])]};
    }

    #[test]
    fn complex_nan() {
        parse_datum!{"1.2-nan.0i",
                     [complex(0,10, [creal(0,3, [coeff(0,3)]),
                                     cimag(3,9, [nan(3,9)])])]};
        parse_datum!{"+nan.0-1.2i",
                     [complex(0,11, [creal(0,6, [nan(0,6)]),
                                     cimag(6,10, [coeff(6,10)])])]};
        parse_datum!{"+nan.0-nan.0i",
                     [complex(0, 13, [creal(0,6,  [nan(0,6)]),
                                      cimag(6,12, [nan(6,12)])])]};
        parse_datum!{"-nan.0+nan.0i",
                     [complex(0, 13, [creal(0,6,  [nan(0,6)]),
                                      cimag(6,12, [nan(6,12)])])]};
        parse_datum!{"+NAN.0-NAN.0I",
                     [complex(0, 13, [creal(0,6,  [nan(0,6)]),
                                      cimag(6,12, [nan(6,12)])])]};
        parse_datum!{"-NAN.0+NAN.0I",
                     [complex(0, 13, [creal(0,6,  [nan(0,6)]),
                                      cimag(6,12, [nan(6,12)])])]};
        parse_datum!{"+inf.0-nan.0i",
                     [complex(0, 13, [creal(0,6,  [inf(0,6)]),
                                      cimag(6,12, [nan(6,12)])])]};
        parse_datum!{"+nan.0-inf.0i",
                     [complex(0, 13, [creal(0,6,  [nan(0,6)]),
                                      cimag(6,12, [inf(6,12)])])]};
    }

    #[test]
    fn complex_implicit_imaginary() {
        parse_datum!{"1.2+i", [complex(0,5, [creal(0,3, [coeff(0,3)]),
                                             cimag(3,4, [plus(3,4)])])]};
        parse_datum!{"1.2-i", [complex(0,5, [creal(0,3, [coeff(0,3)]),
                                             cimag(3,4, [minus(3,4)])])]};
        parse_datum!{"1.2+I", [complex(0,5, [creal(0,3, [coeff(0,3)]),
                                             cimag(3,4, [plus(3,4)])])]};
        parse_datum!{"1.2-I", [complex(0,5, [creal(0,3, [coeff(0,3)]),
                                             cimag(3,4, [minus(3,4)])])]};
        parse_datum!{"1.2e-1+i",
                     [complex(0,8, [creal(0,6, [coeff(0,3), expon(4,6)]),
                                    cimag(6,7, [plus(6,7)])])]};
        parse_datum!{"1.2e-1-i",
                     [complex(0,8, [creal(0,6, [coeff(0,3), expon(4,6)]),
                                    cimag(6,7, [minus(6,7)])])]};
        parse_datum!{"+inf.0+i", [complex(0,8, [creal(0,6, [inf(0,6)]),
                                                cimag(6,7, [plus(6,7)])])]};
        parse_datum!{"+nan.0-i", [complex(0,8, [creal(0,6, [nan(0,6)]),
                                                cimag(6,7, [minus(6,7)])])]};
    }

    #[test]
    fn complex_imaginary_decimal() {
        parse_datum!{"12i",   [complex(0,3, [cimag(0,2, [coeff(0,2)])])]};
        parse_datum!{"1.2i",  [complex(0,4, [cimag(0,3, [coeff(0,3)])])]};
        parse_datum!{"+1.2i", [complex(0,5, [cimag(0,4, [coeff(0,4)])])]};
        parse_datum!{"-1.2i", [complex(0,5, [cimag(0,4, [coeff(0,4)])])]};
        parse_datum!{"123e-2i",
                     [complex(0,7, [cimag(0,6, [coeff(0,3), expon(4,6)])])]};
        parse_datum!{"-123e-2i",
                     [complex(0,8, [cimag(0,7, [coeff(0,4), expon(5,7)])])]};

        parse_datum!{"#d12i",   [complex(0,5, [cimag(2,4, [coeff(2,4)])])]};
        parse_datum!{"#D1.2i",  [complex(0,6, [cimag(2,5, [coeff(2,5)])])]};
        parse_datum!{"#D-12i",  [complex(0,6, [cimag(2,5, [coeff(2,5)])])]};
        parse_datum!{"#d-1.2i", [complex(0,7, [cimag(2,6, [coeff(2,6)])])]};
        parse_datum!{"#d123e-2i",
                     [complex(0,9, [cimag(2,8, [coeff(2,5), expon(6,8)])])]};
        parse_datum!{"#D-123e-2i",
                     [complex(0,10, [cimag(2,9, [coeff(2,6), expon(7,9)])])]};
    }

    #[test]
    fn complex_imaginary_infnan() {
        parse_datum!{"+inf.0i", [complex(0,7, [cimag(0,6, [inf(0,6)])])]};
        parse_datum!{"-inf.0i", [complex(0,7, [cimag(0,6, [inf(0,6)])])]};
        parse_datum!{"+nan.0i", [complex(0,7, [cimag(0,6, [nan(0,6)])])]};
        parse_datum!{"-nan.0i", [complex(0,7, [cimag(0,6, [nan(0,6)])])]};

        parse_datum!{"+INF.0i", [complex(0,7, [cimag(0,6, [inf(0,6)])])]};
        parse_datum!{"-INF.0i", [complex(0,7, [cimag(0,6, [inf(0,6)])])]};
        parse_datum!{"+NAN.0i", [complex(0,7, [cimag(0,6, [nan(0,6)])])]};
        parse_datum!{"-NAN.0i", [complex(0,7, [cimag(0,6, [nan(0,6)])])]};

        parse_datum!{"#d+inf.0i", [complex(0,9, [cimag(2,8, [inf(2,8)])])]};
        parse_datum!{"#D-INF.0i", [complex(0,9, [cimag(2,8, [inf(2,8)])])]};
        parse_datum!{"#d+NAN.0i", [complex(0,9, [cimag(2,8, [nan(2,8)])])]};
        parse_datum!{"#D-nan.0i", [complex(0,9, [cimag(2,8, [nan(2,8)])])]};
    }

    #[test]
    fn complex_lone_i() {
        parse_datum!{"+i",   [complex(0,2, [cimag(0,1, [plus(0,1)])])]};
        parse_datum!{"-i",   [complex(0,2, [cimag(0,1, [minus(0,1)])])]};
        parse_datum!{"#d+i", [complex(0,4, [cimag(2,3, [plus(2,3)])])]};
        parse_datum!{"#d-i", [complex(0,4, [cimag(2,3, [minus(2,3)])])]};

        parse_datum!("i", [ident(0, 1)]);
        parse_datum!("I", [ident(0, 1)]);
    }

    #[test]
    fn polar() {
        parse_datum!{"0@0",       [polar(0,3, [magni(0,1, [coeff(0,1)]),
                                               angle(2,3, [coeff(2,3)])])]};
        parse_datum!{"12@34",     [polar(0,5, [magni(0,2, [coeff(0,2)]),
                                               angle(3,5, [coeff(3,5)])])]};
        parse_datum!{"1.2@3.4",   [polar(0,7, [magni(0,3, [coeff(0,3)]),
                                               angle(4,7, [coeff(4,7)])])]};
        parse_datum!{"-1.2@-3.4", [polar(0,9, [magni(0,4, [coeff(0,4)]),
                                               angle(5,9, [coeff(5,9)])])]};

        parse_datum!{"1.2e+2@-3.4e-1",
                     [polar(0, 14, [magni(0,6,  [coeff(0,3),  expon(4,6)]),
                                    angle(7,14, [coeff(7,11), expon(12,14)])])]};
        parse_datum!{"-1.2E2@4E-1",
                     [polar(0, 11, [magni(0,6,  [coeff(0,4), expon(5,6)]),
                                    angle(7,11, [coeff(7,8), expon(9,11)])])]};

        parse_datum!("@34", [ident(0, 3)]);
    }

    #[test]
    fn polar_infnan() {
        parse_datum!{"+inf.0@1.2", [polar(0,10, [magni(0,6,  [inf(0,6)]),
                                                 angle(7,10, [coeff(7,10)])])]};
        parse_datum!{"-inf.0@1.2", [polar(0,10, [magni(0,6,  [inf(0,6)]),
                                                 angle(7,10, [coeff(7,10)])])]};
        parse_datum!{"1.2@+inf.0", [polar(0,10, [magni(0,3,  [coeff(0,3)]),
                                                 angle(4,10, [inf(4,10)])])]};
        parse_datum!{"1.2@-inf.0", [polar(0,10, [magni(0,3,  [coeff(0,3)]),
                                                 angle(4,10, [inf(4,10)])])]};

        parse_datum!{"+nan.0@1.2", [polar(0,10, [magni(0,6,  [nan(0,6)]),
                                                 angle(7,10, [coeff(7,10)])])]};
        parse_datum!{"-nan.0@1.2", [polar(0,10, [magni(0,6,  [nan(0,6)]),
                                                 angle(7,10, [coeff(7,10)])])]};
        parse_datum!{"1.2@+nan.0", [polar(0,10, [magni(0,3,  [coeff(0,3)]),
                                                 angle(4,10, [nan(4,10)])])]};
        parse_datum!{"1.2@-nan.0", [polar(0,10, [magni(0,3,  [coeff(0,3)]),
                                                 angle(4,10, [nan(4,10)])])]};

        parse_datum!{"+inf.0@+inf.0", [polar(0,13, [magni(0,6,  [inf(0,6)]),
                                                    angle(7,13, [inf(7,13)])])]};
        parse_datum!{"-inf.0@-inf.0", [polar(0,13, [magni(0,6,  [inf(0,6)]),
                                                    angle(7,13, [inf(7,13)])])]};
        parse_datum!{"+inf.0@+nan.0", [polar(0,13, [magni(0,6,  [inf(0,6)]),
                                                    angle(7,13, [nan(7,13)])])]};
        parse_datum!{"+nan.0@+inf.0", [polar(0,13, [magni(0,6,  [nan(0,6)]),
                                                    angle(7,13, [inf(7,13)])])]};
        parse_datum!{"+nan.0@+nan.0", [polar(0,13, [magni(0,6,  [nan(0,6)]),
                                                    angle(7,13, [nan(7,13)])])]};
    }

    #[test]
    fn float() {
        parse_datum!{"0.0",      [float(0, 3, [coeff(0,3)])]};
        parse_datum!{"0.",       [float(0, 2, [coeff(0,2)])]};
        parse_datum!{".0",       [float(0, 2, [coeff(0,2)])]};

        parse_datum!{"+0.0",     [float(0, 4, [coeff(0,4)])]};
        parse_datum!{"+0.",      [float(0, 3, [coeff(0,3)])]};
        parse_datum!{"+.0",      [float(0, 3, [coeff(0,3)])]};

        parse_datum!{"-0.0",     [float(0, 4, [coeff(0,4)])]};
        parse_datum!{"-0.",      [float(0, 3, [coeff(0,3)])]};
        parse_datum!{"-.0",      [float(0, 3, [coeff(0,3)])]};

        parse_datum!{"123.456",  [float(0, 7, [coeff(0,7)])]};
        parse_datum!{"123.",     [float(0, 4, [coeff(0,4)])]};
        parse_datum!{".456",     [float(0, 4, [coeff(0,4)])]};

        parse_datum!{"+123.456", [float(0, 8, [coeff(0,8)])]};
        parse_datum!{"+123.",    [float(0, 5, [coeff(0,5)])]};
        parse_datum!{"+.456",    [float(0, 5, [coeff(0,5)])]};

        parse_datum!{"-123.456", [float(0, 8, [coeff(0,8)])]};
        parse_datum!{"-123.",    [float(0, 5, [coeff(0,5)])]};
        parse_datum!{"-.456",    [float(0, 5, [coeff(0,5)])]};
    }

    #[test]
    fn float_expon() {
        parse_datum!{"12e3",        [float(0,4,  [coeff(0,2), expon(3,4)])]};
        parse_datum!{".1e2",        [float(0,4,  [coeff(0,2), expon(3,4)])]};
        parse_datum!{"1.e2",        [float(0,4,  [coeff(0,2), expon(3,4)])]};
        parse_datum!{"1.234e2",     [float(0,7,  [coeff(0,5), expon(6,7)])]};
        parse_datum!{"12300.0e-2",  [float(0,10, [coeff(0,7), expon(8,10)])]};

        parse_datum!{"+12e3",       [float(0,5,  [coeff(0,3), expon(4,5)])]};
        parse_datum!{"+.1e2",       [float(0,5,  [coeff(0,3), expon(4,5)])]};
        parse_datum!{"+1.e2",       [float(0,5,  [coeff(0,3), expon(4,5)])]};
        parse_datum!{"+1.234e2",    [float(0,8,  [coeff(0,6), expon(7,8)])]};
        parse_datum!{"+12300.0e-2", [float(0,11, [coeff(0,8), expon(9,11)])]};

        parse_datum!{"-12e3",       [float(0,5,  [coeff(0,3), expon(4,5)])]};
        parse_datum!{"-.1e2",       [float(0,5,  [coeff(0,3), expon(4,5)])]};
        parse_datum!{"-1.e2",       [float(0,5,  [coeff(0,3), expon(4,5)])]};
        parse_datum!{"-1.234e2",    [float(0,8,  [coeff(0,6), expon(7,8)])]};
        parse_datum!{"-12300.0e-2", [float(0,11, [coeff(0,8), expon(9,11)])]};
    }

    #[test]
    fn float_radix() {
        parse_datum!{"#d0.0",      [float(0, 5,  [coeff(2,5)])]};
        parse_datum!{"#d123.456",  [float(0, 9,  [coeff(2,9)])]};
        parse_datum!{"#d123.",     [float(0, 6,  [coeff(2,6)])]};
        parse_datum!{"#d.456",     [float(0, 6,  [coeff(2,6)])]};
        parse_datum!{"#d+123.456", [float(0, 10, [coeff(2,10)])]};
        parse_datum!{"#d-123.456", [float(0, 10, [coeff(2,10)])]};
        parse_datum!{"#d-123.4e3", [float(0, 10, [coeff(2,8), expon(9,10)])]};

        parse_datum!{"#D0.0",      [float(0, 5,  [coeff(2,5)])]};
        parse_datum!{"#D123.456",  [float(0, 9,  [coeff(2,9)])]};
        parse_datum!{"#D123.",     [float(0, 6,  [coeff(2,6)])]};
        parse_datum!{"#D.456",     [float(0, 6,  [coeff(2,6)])]};
        parse_datum!{"#D+123.456", [float(0, 10, [coeff(2,10)])]};
        parse_datum!{"#D-123.456", [float(0, 10, [coeff(2,10)])]};
        parse_datum!{"#D-123.4e3", [float(0, 10, [coeff(2,8), expon(9,10)])]};
    }

    #[test]
    fn float_infnan() {
        parse_datum!{"+inf.0", [float(0,6, [inf(0,6)])]}
        parse_datum!{"-inf.0", [float(0,6, [inf(0,6)])]}
        parse_datum!{"+nan.0", [float(0,6, [nan(0,6)])]}
        parse_datum!{"-nan.0", [float(0,6, [nan(0,6)])]}

        parse_datum!{"+INF.0", [float(0,6, [inf(0,6)])]}
        parse_datum!{"-INF.0", [float(0,6, [inf(0,6)])]}
        parse_datum!{"+NAN.0", [float(0,6, [nan(0,6)])]}
        parse_datum!{"-NAN.0", [float(0,6, [nan(0,6)])]}

        parse_datum!{"#d+inf.0", [float(0,8, [inf(2,8)])]}
        parse_datum!{"#d-inf.0", [float(0,8, [inf(2,8)])]}
        parse_datum!{"#d+nan.0", [float(0,8, [nan(2,8)])]}
        parse_datum!{"#d-nan.0", [float(0,8, [nan(2,8)])]}
    }

    #[test]
    fn ident_initial() {
        parse_datum!{"A", [ident(0, 1)]};
        parse_datum!{"a", [ident(0, 1)]};
        parse_datum!{"Z", [ident(0, 1)]};
        parse_datum!{"z", [ident(0, 1)]};
        parse_datum!{"!", [ident(0, 1)]};
        parse_datum!{"$", [ident(0, 1)]};
        parse_datum!{"%", [ident(0, 1)]};
        parse_datum!{"*", [ident(0, 1)]};
        parse_datum!{"/", [ident(0, 1)]};
        parse_datum!{":", [ident(0, 1)]};
        parse_datum!{"<", [ident(0, 1)]};
        parse_datum!{"=", [ident(0, 1)]};
        parse_datum!{">", [ident(0, 1)]};
        parse_datum!{"?", [ident(0, 1)]};
        parse_datum!{"@", [ident(0, 1)]};
        parse_datum!{"^", [ident(0, 1)]};
        parse_datum!{"_", [ident(0, 1)]};
        parse_datum!{"~", [ident(0, 1)]};
    }

    #[test]
    fn ident_simple() {
        parse_datum!{"lambda", [ident(0, 6)]};
        parse_datum!{"a->b",   [ident(0, 4)]};
        parse_datum!{"You+Me", [ident(0, 6)]};
        parse_datum!{"atan2",  [ident(0, 5)]};
        parse_datum!{"q34kTs", [ident(0, 6)]};
        parse_datum!{"<=>",    [ident(0, 3)]};
        parse_datum!{"~_^..?", [ident(0, 6)]};
        parse_datum!{"@snail", [ident(0, 6)]};
        parse_datum!{"*muff*", [ident(0, 6)]};
    }

    #[test]
    fn ident_dot() {
        parse_datum!{"..",   [ident(0, 2)]};
        parse_datum!{"...",  [ident(0, 3)]};
        parse_datum!{".foo", [ident(0, 4)]};
    }

    #[test]
    fn ident_sign() {
        parse_datum!{"+",        [ident(0, 1)]};
        parse_datum!{"-",        [ident(0, 1)]};
        parse_datum!{"+++",      [ident(0, 3)]};
        parse_datum!{"---",      [ident(0, 3)]};
        parse_datum!{"+@3+",     [ident(0, 4)]};
        parse_datum!{"+BOOM+",   [ident(0, 6)]};
        parse_datum!{"->strong", [ident(0, 8)]};
    }

    #[test]
    fn ident_sign_dot() {
        parse_datum!{"+.+",   [ident(0, 3)]};
        parse_datum!{"+.<?",  [ident(0, 4)]};
        parse_datum!{"+.foo", [ident(0, 5)]};
        parse_datum!{"-.-",   [ident(0, 3)]};
        parse_datum!{"-.<?",  [ident(0, 4)]};
        parse_datum!{"-.foo", [ident(0, 5)]};
    }

    #[test]
    fn int2() {
        parse_datum!{"#b0",    [int(0,3, [int2(2,3)])]};
        parse_datum!{"#b+0",   [int(0,4, [int2(2,4)])]};
        parse_datum!{"#b-0",   [int(0,4, [int2(2,4)])]};
        parse_datum!{"#b101",  [int(0,5, [int2(2,5)])]};
        parse_datum!{"#b+101", [int(0,6, [int2(2,6)])]};
        parse_datum!{"#b-101", [int(0,6, [int2(2,6)])]};

        parse_datum!{"#B0",    [int(0,3, [int2(2,3)])]};
        parse_datum!{"#B+0",   [int(0,4, [int2(2,4)])]};
        parse_datum!{"#B-0",   [int(0,4, [int2(2,4)])]};
        parse_datum!{"#B101",  [int(0,5, [int2(2,5)])]};
        parse_datum!{"#B+101", [int(0,6, [int2(2,6)])]};
        parse_datum!{"#B-101", [int(0,6, [int2(2,6)])]};
    }

    #[test]
    fn int8() {
        parse_datum!{"#o0",        [int(0,3,  [int8(2,3)])]};
        parse_datum!{"#o+0",       [int(0,4,  [int8(2,4)])]};
        parse_datum!{"#o-0",       [int(0,4,  [int8(2,4)])]};
        parse_datum!{"#o1234567",  [int(0,9,  [int8(2,9)])]};
        parse_datum!{"#o+1234567", [int(0,10, [int8(2,10)])]};
        parse_datum!{"#o-1234567", [int(0,10, [int8(2,10)])]};

        parse_datum!{"#O0",        [int(0,3,  [int8(2,3)])]};
        parse_datum!{"#O+0",       [int(0,4,  [int8(2,4)])]};
        parse_datum!{"#O-0",       [int(0,4,  [int8(2,4)])]};
        parse_datum!{"#O1234567",  [int(0,9,  [int8(2,9)])]};
        parse_datum!{"#O+1234567", [int(0,10, [int8(2,10)])]};
        parse_datum!{"#O-1234567", [int(0,10, [int8(2,10)])]};
    }

    #[test]
    fn int10() {
        parse_datum!{"0",    [int(0,1, [int10(0,1)])]};
        parse_datum!{"123",  [int(0,3, [int10(0,3)])]};

        parse_datum!{"+0",   [int(0,2, [int10(0,2)])]};
        parse_datum!{"-0",   [int(0,2, [int10(0,2)])]};
        parse_datum!{"+123", [int(0,4, [int10(0,4)])]};
        parse_datum!{"-123", [int(0,4, [int10(0,4)])]};

        parse_datum!{"9223372036854775807",  [int(0,19, [int10(0,19)])]};
        parse_datum!{"+9223372036854775807", [int(0,20, [int10(0,20)])]};
        parse_datum!{"-9223372036854775808", [int(0,20, [int10(0,20)])]};

        parse_datum!{
            "123456789012345678901234567890123456789012345678901234567890",
            [int(0,60, [int10(0,60)])]
        };
    }

    #[test]
    fn int10_radix() {
        parse_datum!{"#d0",    [int(0,3, [int10(2,3)])]};
        parse_datum!{"#d123",  [int(0,5, [int10(2,5)])]};
        parse_datum!{"#d+0",   [int(0,4, [int10(2,4)])]};
        parse_datum!{"#d-0",   [int(0,4, [int10(2,4)])]};
        parse_datum!{"#d+123", [int(0,6, [int10(2,6)])]};
        parse_datum!{"#d-123", [int(0,6, [int10(2,6)])]};

        parse_datum!{"#D0",    [int(0,3, [int10(2,3)])]};
        parse_datum!{"#D123",  [int(0,5, [int10(2,5)])]};
        parse_datum!{"#D+0",   [int(0,4, [int10(2,4)])]};
        parse_datum!{"#D-0",   [int(0,4, [int10(2,4)])]};
        parse_datum!{"#D+123", [int(0,6, [int10(2,6)])]};
        parse_datum!{"#D-123", [int(0,6, [int10(2,6)])]};
    }

    #[test]
    fn int16() {
        parse_datum!{"#x0",       [int(0,3, [int16(2,3)])]};
        parse_datum!{"#xABC123",  [int(0,8, [int16(2,8)])]};
        parse_datum!{"#xabc123",  [int(0,8, [int16(2,8)])]};
        parse_datum!{"#xBadF00d", [int(0,9, [int16(2,9)])]};
        parse_datum!{"#x+0",      [int(0,4, [int16(2,4)])]};
        parse_datum!{"#x-0",      [int(0,4, [int16(2,4)])]};
        parse_datum!{"#x+abc123", [int(0,9, [int16(2,9)])]};
        parse_datum!{"#x-abc123", [int(0,9, [int16(2,9)])]};

        parse_datum!{"#X0",       [int(0,3, [int16(2,3)])]};
        parse_datum!{"#XABC123",  [int(0,8, [int16(2,8)])]};
        parse_datum!{"#Xabc123",  [int(0,8, [int16(2,8)])]};
        parse_datum!{"#XBadF00d", [int(0,9, [int16(2,9)])]};
        parse_datum!{"#X+0",      [int(0,4, [int16(2,4)])]};
        parse_datum!{"#X-0",      [int(0,4, [int16(2,4)])]};
        parse_datum!{"#X+abc123", [int(0,9, [int16(2,9)])]};
        parse_datum!{"#X-abc123", [int(0,9, [int16(2,9)])]};
    }


    #[test]
    fn list_empty() {
        parse_datum!{"()",     [list(0, 2, [])]};
        parse_datum!{"(  )",   [list(0, 4, [])]};
        parse_datum!{"(\t\t)", [list(0, 4, [])]};
        parse_datum!{"(\n\n)", [list(0, 4, [])]};
        parse_datum!{"(\r\n)", [list(0, 4, [])]};
    }

    #[test]
    fn list_flat() {
        parse_datum!{"(a)", [list(0, 3, [ident(1, 2)])]};
        parse_datum!{
            "(if #t\n\tx)",
            [list(0, 10, [ident(1, 3), btrue(4, 6), ident(8, 9)])]
        };
    }

    #[test]
    fn list_nested() {
        parse_datum!{"((()))", [list(0, 6, [list(1, 5, [list(2, 4, [])])])]};
        parse_datum!{
            "(if (a)\n\tb)",
            [list(0, 11, [ident(1, 3), list(4, 7, [ident(5, 6)]), ident(9, 10)])]
        };
    }

    #[test]
    fn list_smushed() {
        parse_datum!{
            "(if(a)b)",
            [list(0, 8, [ident(1, 3), list(3, 6, [ident(4, 5)]), ident(6, 7)])]
        };
        parse_datum!{
            "(#t()#f)",
            [list(0, 8, [btrue(1, 3), list(3, 5), bfalse(5,7)])]
        };
    }

    #[test]
    fn dotlist() {
        parse_datum!{
            "(a . b)",
            [list(0, 7, [ident(1,2), dot(3, 4), ident(5,6)])]
        };
        parse_datum!{
            "(a b . c)",
            [list(0, 9, [ident(1,2), ident(3,4), dot(5,6), ident(7,8)])]
        };
    }

    #[test]
    fn bad_dotlist() {
        parse_datum!{"(.)",   [list(0, 3, [dot(1,2)])]};
        parse_datum!{"( . )", [list(0, 5, [dot(2,3)])]};

        parse_datum!{"(. a)",  [list(0, 5, [dot(1,2), ident(3,4)])]};
        parse_datum!{"( . a)", [list(0, 6, [dot(2,3), ident(4,5)])]};

        parse_datum!{
            "(a . b c)",
            [list(0, 9, [ident(1,2), dot(3,4), ident(5,6), ident(7,8)])]
        };
        parse_datum!{
            "(a b . c d)",
            [list(0, 11, [ident(1,2), ident(3,4), dot(5,6), ident(7,8), ident(9,10)])]
        };

        parse_datum!{
            "(a . b . c)",
            [list(0, 11, [ident(1,2), dot(3,4), ident(5,6), dot(7,8), ident(9,10)])]
        };
        parse_datum!{"(. . .)", [list(0, 7, [dot(1,2), dot(3,4), dot(5,6)])]};
    }


    #[test]
    fn ratio_no_radix() {
        parse_datum!{"2/3",   [ratio(0,3, [int10(0,1), int10(2,3)])]};
        parse_datum!{"-2/+3", [ratio(0,5, [int10(0,2), int10(3,5)])]};
        parse_datum!{"+2/-3", [ratio(0,5, [int10(0,2), int10(3,5)])]};
        parse_datum!{
            "+1234567890/+9876543210",
            [ratio(0,23, [int10(0,11), int10(12,23)])]
        };

        parse_datum!{"1/0", [ratio(0,3, [int10(0,1), int10(2,3)])]};
    }

    #[test]
    fn ratio2() {
        parse_datum!{"#b101/111", [ratio(0,9, [int2(2,5), int2(6,9)])]};
        parse_datum!{"#B101/111", [ratio(0,9, [int2(2,5), int2(6,9)])]};
    }

    #[test]
    fn ratio8() {
        parse_datum!{"#o123/456", [ratio(0,9, [int8(2,5), int8(6,9)])]};
        parse_datum!{"#O123/456", [ratio(0,9, [int8(2,5), int8(6,9)])]};
    }

    #[test]
    fn ratio10() {
        parse_datum!{"#d123/789", [ratio(0,9, [int10(2,5), int10(6,9)])]};
        parse_datum!{"#D123/789", [ratio(0,9, [int10(2,5), int10(6,9)])]};
    }

    #[test]
    fn ratio16() {
        parse_datum!{"#x123/abc", [ratio(0,9, [int16(2,5), int16(6,9)])]};
        parse_datum!{"#X123/abc", [ratio(0,9, [int16(2,5), int16(6,9)])]};
    }

    #[test]
    fn bad_ratio() {
        parse_datum!{
            "(1/2)",
            [list(0,5, [ratio(1,4, [int10(1,2), int10(3,4)])])]
        };
        parse_datum!{
            "(1 /2)",
            [list(0,6, [int(1,2, [int10(1,2)]), ident(3,5)])]
        };
        parse_datum!{
            "(1 / 2)",
            [list(0,7, [int(1,2, [int10(1,2)]), ident(3,4), int(5,6, [int10(5,6)])])]
        };
    }


    #[test]
    fn string_empty() {
        parse_datum!{r#""""#, [string(0, 2, [])]};
    }

    #[test]
    fn string_ascii_chars() {
        parse_datum!{r#""a""#,       [string(0, 3, [chars(1,2)])]};
        parse_datum!{r#""foo bar""#, [string(0, 9, [chars(1,8)])]};
        parse_datum!{"\"foo\nbar\"", [string(0, 9, [chars(1,8)])]};
    }

    #[test]
    fn string_unicode_chars() {
        parse_datum!{r#""🐱""#, [string(0, 6,  [chars(1,5)])]};
        parse_datum!{r#""日本語""#, [string(0, 11, [chars(1,10)])]};
        parse_datum!{r#""λάμβδα""#, [string(0, 14, [chars(1,13)])]};
    }

    #[test]
    fn string_basic_esc() {
        parse_datum!{r#""\a""#, [string(0, 4, [esc(1,3)])]};
        parse_datum!{r#""\b""#, [string(0, 4, [esc(1,3)])]};
        parse_datum!{r#""\t""#, [string(0, 4, [esc(1,3)])]};
        parse_datum!{r#""\n""#, [string(0, 4, [esc(1,3)])]};
        parse_datum!{r#""\r""#, [string(0, 4, [esc(1,3)])]};
        parse_datum!{r#""\"""#, [string(0, 4, [esc(1,3)])]};
        parse_datum!{r#""\\""#, [string(0, 4, [esc(1,3)])]};
        parse_datum!{r#""\|""#, [string(0, 4, [esc(1,3)])]};
    }

    #[test]
    fn string_hex_esc() {
        parse_datum!{r#""\xa;""#,      [string(0, 6,  [esc(1,5)])]};
        parse_datum!{r#""\x3BB;""#,    [string(0, 8,  [esc(1,7)])]};
        parse_datum!{r#""\x01f5fb;""#, [string(0, 11, [esc(1,10)])]};
        parse_datum!{
            r#""\x65e5;\x672c;\x8a9e;""#,
            [string(0, 23, [esc(1,8), esc(8,15), esc(15,22)])]
        };

        parse_datum!{r#""\x3g3;""#, [string(0, 8, [bad_esc(1,3), chars(3,7)])]};

        parse_datum!{r#""\x3BB""#,  [string(0, 7, [bad_esc(1,3), chars(3,6)])]};
        parse_datum!{r#""\x3BB.""#, [string(0, 8, [bad_esc(1,3), chars(3,7)])]};
    }


    #[test]
    fn string_mixed() {
        parse_datum!{
            r#""(\x3BB;.x)""#,
            [string(0, 12, [chars(1,2), esc(2,8), chars(8,11)])]
        };
    }

    #[test]
    fn escaped_line_spaceless() {
        parse_datum!{
            "\"foo\\\nbar\"",
            [string(0, 10, [chars(1,4), chars(6,9)])]
        };
        parse_datum!{
            "\"foo\\\r\nbar\"",
            [string(0, 11, [chars(1,4), chars(7,10)])]
        };
        parse_datum!{
            "\"foo\\\rbar\"",
            [string(0, 10, [chars(1,4), chars(6,9)])]
        };

        parse_datum!{
            r#""foo\\nbar""#,
            [string(0, 11, [chars(1,4), esc(4,6), chars(6,10)])]
        };
        parse_datum!{
            r#""foo\\r\nbar""#,
            [string(0, 13, [chars(1,4), esc(4,6), chars(6,7), esc(7,9), chars(9,12)])]
        };
        parse_datum!{
            r#""foo\\rbar""#,
            [string(0, 11, [chars(1,4), esc(4,6), chars(6,10)])]
        };
    }

    #[test]
    fn escaped_line_spaces() {
        parse_datum!{
            "\"foo\\  \n  bar\"",
            [string(0, 14, [chars(1,4), chars(10,13)])]
        };
        parse_datum!{
            "\"foo\\  \r\n  bar\"",
            [string(0, 15, [chars(1,4), chars(11,14)])]
        };
        parse_datum!{
            "\"foo\\  \r  bar\"",
            [string(0, 14, [chars(1,4), chars(10,13)])]
        };

        parse_datum!{
            r#""foo\  \n  bar""#,
            [string(0, 15, [chars(1,4), bad_esc(4,6), chars(6,7), esc(7,9), chars(9,14)])]
        };
        parse_datum!{
            r#""foo\  \r\n  bar""#,
            [string(0, 17, [chars(1,4), bad_esc(4,6), chars(6,7),
                            esc(7,9), esc(9,11), chars(11,16)])]
        };
        parse_datum!{
            r#""foo\  \r  bar""#,
            [string(0, 15, [chars(1,4), bad_esc(4,6), chars(6,7), esc(7,9), chars(9,14)])]
        };
    }

    #[test]
    fn escaped_line_tabs() {
        parse_datum!{
            "\"foo\\\t\n\tbar\"",
            [string(0, 12, [chars(1,4), chars(8,11)])]
        };
        parse_datum!{
            "\"foo\\\t\r\n\tbar\"",
            [string(0, 13, [chars(1,4), chars(9,12)])]
        };
        parse_datum!{
            "\"foo\\\t\r\tbar\"",
            [string(0, 12, [chars(1,4), chars(8,11)])]
        };

        parse_datum!{
            r#""foo\\t\n\tbar""#,
            [string(0, 15, [chars(1,4), esc(4,6), chars(6,7),
                            esc(7,9), esc(9,11), chars(11,14)])]
        };
        parse_datum!{
            r#""foo\\t\r\n\tbar""#,
            [string(0, 17, [chars(1,4), esc(4,6), chars(6,7), esc(7,9),
                            esc(9,11), esc(11,13), chars(13,16)])]
        };
        parse_datum!{
            r#""foo\\t\r\tbar""#,
            [string(0, 15, [chars(1,4), esc(4,6), chars(6,7),
                            esc(7,9), esc(9,11), chars(11,14)])]
        };
    }

    #[test]
    fn escaped_line_mixed() {
        parse_datum!{
            "\"foo\\ \t \n \t bar\"",
            [string(0, 16, [chars(1,4), chars(12,15)])]
        };
        parse_datum!{
            "\"foo\\ \t \r\n \t bar\"",
            [string(0, 17, [chars(1,4), chars(13,16)])]
        };
        parse_datum!{
            "\"foo\\ \t \r \t bar\"",
            [string(0, 16, [chars(1,4), chars(12,15)])]
        };

        parse_datum!{
            r#""foo\ \t \n \t bar""#,
            [string(0, 19,
                    [chars(1,4), bad_esc(4,6), esc(6,8), chars(8,9),
                     esc(9,11), chars(11,12), esc(12,14), chars(14, 18)])]
        };
        parse_datum!{
            r#""foo\ \t \r\n \t bar""#,
            [string(0, 21,
                    [chars(1,4), bad_esc(4,6), esc(6,8), chars(8,9), esc(9,11),
                     esc(11,13), chars(13,14), esc(14,16), chars(16, 20)])]
        };
        parse_datum!{
            r#""foo\ \t \r \t bar""#,
            [string(0, 19,
                    [chars(1,4), bad_esc(4,6), esc(6,8), chars(8,9),
                     esc(9,11), chars(11,12), esc(12,14), chars(14, 18)])]
        };
    }

    #[test]
    fn escaped_line_whitespace_before() {
        parse_datum!{
            "\"foo \t \\\n  bar\"",
            [string(0, 15, [chars(1,7), chars(11,14)])]
        };
        parse_datum!{
            "\"foo \t \\\r\n  bar\"",
            [string(0, 16, [chars(1,7), chars(12,15)])]
        };
        parse_datum!{
            "\"foo \t \\\r  bar\"",
            [string(0, 15, [chars(1,7), chars(11,14)])]
        };

        parse_datum!{
            r#""foo \t \\n  bar""#,
            [string(0, 17, [chars(1,5), esc(5,7), chars(7,8), esc(8,10), chars(10,16)])]
        };
        parse_datum!{
            r#""foo \t \\r\n  bar""#,
            [string(0, 19, [chars(1,5), esc(5,7), chars(7,8), esc(8,10),
                            chars(10,11), esc(11,13), chars(13,18)])]
        };
        parse_datum!{
            r#""foo \t \\r  bar""#,
            [string(0, 17, [chars(1,5), esc(5,7), chars(7,8), esc(8,10), chars(10,16)])]
        };
    }


    #[test]
    fn vector_empty() {
        parse_datum!{"#()",     [vector(0, 3)]};
        parse_datum!{"#(  )",   [vector(0, 5)]};
        parse_datum!{"#(\t\t)", [vector(0, 5)]};
        parse_datum!{"#(\n\n)", [vector(0, 5)]};
        parse_datum!{"#(\r\n)", [vector(0, 5)]};
    }

    #[test]
    fn vector_flat() {
        parse_datum!{"#(a)", [vector(0, 4, [ident(2, 3)])]};
        parse_datum!{
            "#(a #t\n\tz)",
            [vector(0, 10, [ident(2, 3), btrue(4, 6), ident(8, 9)])]
        };
    }

    #[test]
    fn vector_nested() {
        parse_datum!{"#(#(#()))", [vector(0, 9, [vector(2, 8, [vector(4, 7)])])]};
        parse_datum!{
            "#(a #(b)\n\tc)",
            [vector(0, 12, [ident(2, 3), vector(4, 8, [ident(6, 7)]), ident(10, 11)])]
        };
    }
}
